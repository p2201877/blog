/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';


// jshint browser:true, eqeqeq:true, undef:true, devel:true, esversion: 6

// Ensemble du script stocké dans une fonction main 
function main() {


	function modeNuit() {
		document.getElementsByTagName('body')[0].classList.add('dark');
		bouton.removeEventListener('click', modeNuit);
		bouton.addEventListener('click', modeJour);
		icone.className = "fas fa-sun";
		bouton.style.flexDirection = 'row-reverse';
	}

	function modeJour() {
		document.getElementsByTagName('body')[0].classList.remove('dark');
		bouton.removeEventListener('click', modeJour);
		bouton.addEventListener('click', modeNuit);
		icone.className = "fas fa-moon";
		bouton.style.flexDirection = 'row';
	}

	const bouton = document.getElementById('switch');
	const icone = document.getElementsByTagName('i')[0];
	bouton.addEventListener('click', modeNuit);


}

// Appel du main
main();