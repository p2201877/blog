# Blog

## Description
Site internet sous forme de blog avec des articles pouvant appartenir à différentes catégories et avoir des commentaires

## Requirements
- PHP 8.2.0
- Node.js
- Composer

## Installation
```
composer install
php bin/console do:mi:mi
npm install
npm run dev
npm run build
npm run watch
```

## Etat du projet
En cours de développement